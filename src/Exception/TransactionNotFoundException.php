<?php

namespace Drupal\commerce_payment_sync\Exception;

/**
 * Class TransactionNotFoundException.
 *
 * @package commerce_payment_sync\Exception
 */
class TransactionNotFoundException extends \Exception {

}
