<?php

namespace Drupal\commerce_payment_sync\Plugin\Action;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface;
use Drupal\commerce_payment_sync\Exception\EmptyRemoteOrderIdException;
use Drupal\commerce_payment_sync\Exception\TransactionNotCompletedException;
use Drupal\commerce_payment_sync\Exception\TransactionNotFoundException;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a a Commerce Payment Sync action.
 *
 * @Action(
 *   id = "commerce_payment_sync",
 *   label = @Translation("Commerce Payment Sync"),
 *   type = "commerce_order",
 *   category = @Translation("Commerce")
 * )
 */
class CommercePaymentSync extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;

  /**
   * The commerce payment sync manager service.
   *
   * @var \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface
   */
  protected $commercePaymentSyncManager;

  /**
   * CommerceSermepaPaymentSync constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface $commercePaymentSyncManager
   *   The commerce payment sync manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CommercePaymentSyncManagerInterface $commercePaymentSyncManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->commercePaymentSyncManager = $commercePaymentSyncManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_payment_sync.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($commerce_order, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $commerce_order */
    $commerce_access = $commerce_order->access('update', $account, TRUE);
    // Only allow to perform this action if is order is pending.
    if ($this->commercePaymentSyncManager->isOrderPending($commerce_order)) {
      $access = $commerce_access;
    }
    else {
      $this->messenger()
        ->addWarning($this->t('The order :order_id cannot be processed because it is not a pending order', [':order_id' => $commerce_order->id()]));
      $access = new AccessResultForbidden();
      $access->inheritCacheability($commerce_access);
    }
    return $return_as_object ? $access : !$access->isForbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function execute($commerce_order = NULL) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $commerce_order */
    if ($commerce_order instanceof OrderInterface) {
      try {
        $this->commercePaymentSyncManager->processOrder($commerce_order);
        return;
      }
      catch (EmptyRemoteOrderIdException $e) {
        $message = $this->t("The order :order_id can't be processed because it has no gateway_order_id stored",
          [':order_id' => $commerce_order->id()]);
      }
      catch (TransactionNotFoundException $e) {
        $message = $this->t("The order :order_id can't be found in the payment gateway. Depending on the order date you can considerate to set the order as cancelled.",
          [':order_id' => $commerce_order->id()]);
      }
      catch (TransactionNotCompletedException $e) {
        $message = $this->t("The order :order_id is unpaid. Depending on the order date you can considerate to set the order as cancelled.",
          [':order_id' => $commerce_order->id()]);
      }
      catch (\Exception $e) {
        $message = $this->t("An unknown error happened on processing the order :order_id. Check the logs to get more information.",
          [':order_id' => $commerce_order->id()]);
      }
      $this->messenger()->addError($message);
      watchdog_exception('commerce_payment_sync', $e);
    }
  }

}
